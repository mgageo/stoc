# <!-- coding: utf-8 -->
#
# la partie carrés
# auteur : Marc Gauthier
# licence: Creative Commons Paternité - Pas d"Utilisation Commerciale - Partage des Conditions Initiales à l"Identique 2.0 France
#
#
# les observateurs par carré
# source("geo/scripts/stoc.R"); carres_jour(1)
carres_jour <- function(test=2) {
  carp()
  library(compareDF)
  df1 <- carres_mga() %>%
    glimpse()
  champs <- colnames(df1) %>%
    glimpse()
  df2 <- carres_faune() %>%
    glimpse()
  df3 <- carres_slh()
  df4 <- carres_mnhn() %>%
    dplyr::select(champs) %>%
    glimpse()
  df0 <- compare_df(df1, df4, champs)
  dsn <- sprintf("%s/%s_vs_%s.xlsx", varDir, "mga", "mnhn")
  create_output_table(df0,
    output_type = "xlsx",
    file_name = dsn,
    limit = 20000,
    color_scheme = c(addition = "#52854C", removal = "#FC4E07", unchanged_cell = "#999999", unchanged_row = "#293352"),
    headers = NULL,
    change_col_name = "chng_type",
    group_col_name = "grp"
  )
  tex_pdflatex("carres.tex")
}
# source("geo/scripts/stoc.R"); carres_jour_slh(1)
carres_jour_slh <- function(test=2) {
  carp()
  library(compareDF)
  mga.df <- carres_mga() %>%
    dplyr::rename(carre = carré) %>%
    glimpse()
  dsn <- sprintf("%s/SergeLeHuitouze/Stoceurs.xlsx", varDir)
  slh.df <- rio::import(dsn) %>%
    clean_names() %>%
    dplyr::rename(carre = n_carre) %>%
    glimpse()
  mnhn.df <- mnhn_brut_lire() %>%
#    glimpse() %>%
    distinct(carre, annee, observateur) %>%
    glimpse()
#
# pour vérif visuelle
  carp("les carrés mnhn sans email mga")
  df1 <- mnhn.df %>%
    distinct(carre, observateur) %>%
    left_join(mga.df, by = c("carre"))
  df1 %>%
    filter(is.na(email)) %>%
    glimpse()
  View(df1)
# comparaison avec fichier mga sur carre, email
  df1 <- mga.df %>%
    dplyr::select(carre, email)
  carp("les carrés de Serge")
  df2 <- slh.df %>%
    distinct(carre, email) %>%
    glimpse()
  df0 <- compare_df(df1, df2, colnames(df1))
  dsn <- sprintf("%s/%s_vs_%s.xlsx", varDir, "mga", "slh")
  create_output_table(df0,
    output_type = "xlsx",
    file_name = dsn,
    limit = 20000,
    color_scheme = c(addition = "#52854C", removal = "#FC4E07", unchanged_cell = "#999999", unchanged_row = "#293352"),
    headers = NULL,
    change_col_name = "chng_type",
    group_col_name = "grp"
  )
  carp("dsn: %s", dsn)
}
#
# la version de faune-bretagne 2015 -> ...
carres_faune <- function() {
  carp()
  library(tidyverse)
  df <- faune_proto_lire() %>%
    group_by(carré = id_ref, prénom = surname, nom = name) %>%
    summarize(nb = n()) %>%
    ungroup()
  dossier <- "carres"
  dossierDir <- sprintf("%s/%s", texDir, dossier);
  dir.create(dossierDir, showWarnings = FALSE, recursive = TRUE)
  tex_df2kable(df, num = TRUE, dossier = dossier)
  return(invisible(df))
}
#
# ma version
carres_mga <- function() {
  carp()
  library(tidyverse)
  library(rio)
  dsn <- sprintf("%s/mga.xlsx", varDir)
  df <- rio::import(dsn)
  dossier <- "carres"
  dossierDir <- sprintf("%s/%s", texDir, dossier);
  dir.create(dossierDir, showWarnings = FALSE, recursive = TRUE)
  tex_df2kable(df, num = TRUE, dossier = dossier)
  return(invisible(df))
}
# la version données MNHN
carres_mnhn <- function() {
  carp()
  library(tidyverse)
  library(rio)
  df <- mnhn_brut_lire() %>%
    group_by(carré = carre, prénom = observateur, nom = observateur) %>%
    summarize(nb = n()) %>%
    ungroup()
  dossier <- "carres"
  dossierDir <- sprintf("%s/%s", texDir, dossier);
  dir.create(dossierDir, showWarnings = FALSE, recursive = TRUE)
  tex_df2kable(df, num = TRUE, dossier = dossier)
  return(invisible(df))
}
#
# la version de Serge Le Huitouze
carres_slh <- function() {
  carp()
  library(tidyverse)
  library(rio)
  library(janitor)
  dsn <- sprintf("%s/SergeLeHuitouze/Stoceurs.xlsx", varDir)
  df <- rio::import(dsn) %>%
    clean_names() %>%
    dplyr::select(carré = n_carre, prénom = prenom, nom) %>%
    arrange(carré)
  dossier <- "carres"
  dossierDir <- sprintf("%s/%s", texDir, dossier);
  dir.create(dossierDir, showWarnings = FALSE, recursive = TRUE)
  tex_df2kable(df, num = TRUE, dossier = dossier)
  return(invisible(df))
}