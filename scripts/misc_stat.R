# <!-- coding: utf-8 -->
#
# la partie stat
# auteur : Marc Gauthier
# licence: Creative Commons Paternité - Pas d'Utilisation Commerciale - Partage des Conditions Initiales à l'Identique 2.0 France
# ===============================================================
#
#
# pour faire des stat sur un champ
stat_champ <- function(df, champ = "NAME_SPECIES") {
  nb.df <- data.frame(table(df[, champ]), stringsAsFactors=FALSE)
  colnames(nb.df) <- c("critere", "nb")
  nb.df <- subset(nb.df, nb > 0 )
#  nb.df$critere <- as.character(nb.df$critere)
#  print(head(nb.df,20))
  nb <- sum(nb.df$nb)
  nb.df$critere <- iconv(nb.df$critere, "UTF-8")
#
#  print(nb.df$critere)
  p <- ggplot(data=nb.df, aes(x=critere, y=nb)) +
    coord_flip() +
    scale_x_discrete("") +
    ylab(sprintf("nb : %s", nb)) +
    geom_bar(stat="identity", position=position_dodge(), colour="black")
  print(p)
  return(p)
}
stat_champ_somme <- function(df, champ = "NAME_SPECIES", nombre = "TOTAL_COUNT") {
  df[,c(nombre)] <- as.numeric(as.character(df[,c(nombre)] ))
  nb.df <- aggregate(df[,c(nombre)], by=list(df[,c(champ)]), FUN=sum)
  colnames(nb.df) <- c("critere", "nb")
  nb <- sum(nb.df$nb)
  nb.df$critere <- iconv(nb.df$critere, "UTF-8")
#
#  print(nb.df$critere)
  p <- ggplot(data=nb.df, aes(x=critere, y=nb)) +
    coord_flip() +
    scale_x_discrete("") +
    ylab(sprintf("nb : %s", nb)) +
    geom_bar(stat="identity", position=position_dodge(), colour="black")
  print(p)
  return(p)
}
stat_champs <- function(df, champs, champ="nb") {
  df <- data.frame(table(df[, champs]), stringsAsFactors=FALSE)
  colnames(df) <- append(champs, "nb")
  df <- subset(df, nb > 0 )
  colnames(df) <- append(champs, champ)
  return(df)
}
stat_champs_somme <- function(df, champs, champ="nb", nombre = "TOTAL_COUNT") {
  df[,c(nombre)] <- as.numeric(as.character(df[,c(nombre)] ))
  zz <- list()
  for(i in 1:length(champs)) {
    zz[[champs[i]]] <- df[, champs[i]]
  }
  df <- aggregate(df[,c(nombre)], by=zz, FUN=sum)
  colnames(df) <- append(champs, champ)
  return(df)
}
stat_percent <- function(df, champs, total="nb") {
  for ( champ in champs ) {
    champ_pc <- sprintf("%s_pc", champ)
    df[, champ_pc] <- round(100*df[, champ]/df[, total],0)
  }
  return(df)
}
stat_percent_col <- function(df, total, champ, champ_pc) {
  df[, champ_pc] <- round(100*df[, champ]/df[, total],0)
  return(df)
}
stat_percent_total <- function(df, champs, total="nb") {
  total.df <- data.frame(t(colSums(df[, champs])))
  print(total.df);

  df <- rbind(df, df[1,])
  df[nrow(df), ] <- ''
  df[nrow(df), 1] <- 'total'
  df[nrow(df), champs] <- total.df[1, ]
  for ( champ in champs ) {
#    print(sprintf("stat_percent_total() champ : %s", champ))
    champ_pc <- sprintf("%s_pc", champ)
    df[, champ] <- as.numeric(df[, champ])
    df[, champ_pc] <- round(100*df[, champ]/total.df[, champ],0)
  }
  return(df)
}
stat_total <- function(df, champs) {
  total.df <- data.frame(t(colSums(df[, champs])))
  df <- rbind(df, df[1,])
  df[nrow(df), ] <- ''
  df[nrow(df), 1] <- 'total'
  df[nrow(df), champs] <- total.df[1, ]
  return(df)
}
stat_total1 <- function(df) {
  champs <- colnames(df)
  champs <- champs[2:length(champs)]
  total.df <- data.frame(t(colSums(df[, champs])))
  df <- rbind(df, df[1,])
  df[nrow(df), ] <- ''
  df[nrow(df), 1] <- 'total'
  df[nrow(df), champs] <- total.df[1, ]
  return(df)
}
#
# graphique avec ggplot
#
ggplot_flip <- function(df) {
  df$espece <- factor(df$espece, levels = df$espece[order(df$nb)])
  gg <- ggplot(data=df, aes(x=espece, y=nb)) +
    geom_bar(stat="identity", width=0.7) +
    scale_y_continuous( limits = c(0, 5) ) +
    theme.blank +
    scale_fill_manual(values=c("#e75e00", "#009e73", "#56b4e9")) +
    theme(legend.position = c(0.8, 0.2)) +
    theme(legend.title = element_blank()) +
#    annotate("text", x = c(2), y = c(60), label = nbj , color="black", size=5, fontface="bold") +
    coord_flip()
  print(gg)
  return(invisible(gg))
}
#
# part d'un département suivant les années
#
stat_dept_anneee_frequence <- function(df, dossier = "test") {
  carp()
  library(tidyverse)
  library(ggplot2)
  annees <- distinct(df, annee)
  df1 <- df %>%
    group_by(annee, dept) %>%
    summarise(nb = n())
  df2 <- df1 %>%
    group_by(dept) %>%
    mutate(total = sum(nb)) %>%
    spread(annee, nb, 0) %>%
    arrange(desc(total)) %>%
    filter(total > 0)
#  df2 <- df2[1:50, ]
  tex_df2kable(df2, dossier = dossier, nb_lignes = 60, num = TRUE)
  df2 <- df1 %>%
    group_by(annee) %>%
    mutate(totcells = sum(nb), # how many cells overall
      percent = round(1000 * nb / totcells, 0)) %>% #cells by landuse / total cells
    dplyr::select(-c(nb, totcells)) %>%
    spread(key = annee, value = percent, fill = 0)
  tex_df2kable(df2, dossier = dossier, suffixe = "pm", nb_lignes = 60, num = TRUE)
# le poids d'une espèce sur l'ensemble des espèces
  df2 <- df1 %>%
    group_by(annee) %>%
    mutate(totcells = sum(nb), # how many cells overall
      percent = round(1000 * nb / totcells, 0)) %>% #cells by landuse / total cells
    dplyr::select(-c(nb, totcells)) %>%
    print(n=30)
  gg <- ggplot(df2, aes(x = annee, y = percent, group = dept, colour = dept)) +
    geom_line(size = 3, aes(color=dept)) +
    geom_text(aes(label = percent), hjust = 0, vjust = 0, color = "black", check_overlap = TRUE) +
    scale_y_continuous(
      name="0/00",
    )
  theme_gg <- theme(
    panel.background = element_rect(fill = "grey"),
    plot.background = element_rect(fill = "grey"),
    axis.title.x = element_blank(),
  )
  gg <- gg + theme_gg
  X <- max(df2$annee)
  Y <- max(df2$percent)
  gg <- gg +
    annotate("text", x = X, y = Y, label = "@ Marc Gauthier CC-BY-NC-ND"
    , hjust=1, vjust=1, col="white", cex=3,  fontface = "bold", alpha = 0.8
    )
  plot(gg)
  ggpdf(gg, dossier = dossier)
  return(invisible(gg))
}
#
# part d'une espèce suivant les années
#
stat_espece_anneee_frequence <- function(df, dossier = "test") {
  carp()
  library(tidyverse)
  library(ggplot2)
  library(broom)
  annees <- distinct(df, annee)
  df1 <- df %>%
    group_by(annee, espece) %>%
    summarise(nb = n())
  df2 <- df1 %>%
    group_by(espece) %>%
    mutate(total = sum(nb)) %>%
    spread(annee, nb, 0) %>%
    arrange(desc(total)) %>%
    filter(total > 0)
#  df2 <- df2[1:50, ]
  tex_df2kable(df2, dossier = dossier, nb_lignes = 60, num = TRUE)
  carp("calcul du poids d'une espèce")
  df2 <- df1 %>%
    group_by(annee) %>%
    mutate(
      totcells = sum(nb),
      percent = round(1000 * nb / totcells, 0)
    ) %>%
    dplyr::select(-c(nb, totcells))
  export_df2xlsx(df2)
# passage en large pour le pdf
  carp("passage en large")
  df21 <- df2 %>%
    spread(key = annee, value = percent, fill = 0)
  tex_df2kable(df21, dossier = dossier, suffixe = "pm", nb_lignes = 60, num = TRUE)
  df22 <- stat_espece_anneee_tendance(df21)
  tex_df2kable(df22, dossier = dossier, suffixe = "tendance", nb_lignes = 60, num = TRUE, digits = 1)
#
# la régression
  df3 <- df2 %>%
    ungroup() %>%
    glimpse() %>%
    arrange(desc(annee), desc(percent))
  df3 <- df3[1:50, ] %>%
    ungroup() %>%
    glimpse()
  df4 <- df2 %>%
    filter(espece %in% df3$espece)
  df5 <- df4 %>%
    nest(data = -espece) %>%
    mutate(
      fit = map(data, ~ lm(annee ~ percent, data = .x)),
      tidied = map(fit, tidy)
    ) %>%
    unnest(tidied) %>%
    glimpse() %>%
    filter(term == "percent") %>%
    filter(! is.na(p.value)) %>%
    dplyr::select(-data, -fit, -term) %>%
    arrange(espece)
  tex_df2kable(df5, dossier = dossier, suffixe = "lm", nb_lignes = 60, num = TRUE, font_size = 9)
#
# pour le graphique avec les espèces les plus fréquantes
  df3 <- df2 %>%
    ungroup() %>%
    glimpse() %>%
    arrange(desc(annee), desc(percent))
  df3 <- df3[1:6, ] %>%
    ungroup() %>%
    glimpse()
  df4 <- df2 %>%
    filter(espece %in% df3$espece) %>%
    glimpse() %>%
    print(n = 20)
  gg <- ggplot(df4, aes(x = annee, y = percent, group = espece, colour = espece)) +
    geom_line(size = 3, aes(color=espece)) +
    geom_smooth(method = "lm") +
    geom_text(aes(label = percent), hjust = 0, vjust = 0, color = "black", check_overlap = TRUE) +
    scale_y_continuous(
      name = "0/00",
    )
  theme_gg <- theme(
    panel.background = element_rect(fill = "grey"),
    plot.background = element_rect(fill = "grey"),
    axis.title.x = element_blank(),
  )
  gg <- gg + theme_gg
  X <- max(df4$annee)
  Y <- min(df4$percent)
  gg <- gg +
    annotate("text", x = X, y = Y, label = "@ Marc Gauthier CC-BY-NC-ND"
    , hjust=1, vjust=1, col="white", cex=3,  fontface = "bold", alpha = 0.8
    )
  plot(gg)
  ggpdf(gg, dossier = dossier)
  return(invisible(gg))
}
#
# calcul de la tendance de la dernière année
#
# détermination des moyennes, écart-type, intervalle de tolérance
stat_espece_anneee_tendance <- function(df) {
  carp()
# https://www.gertstulp.com/post/different-ways-of-calculating-rowmeans-on-selected-variables-in-a-tidyverse-framework/
  select_vars <- head(grep("^20", colnames(df)), -1) %>%
    glimpse()
  derniere <- rev(colnames(df))[1] %>%
    glimpse()
  df <- df %>%
    glimpse() %>%
    mutate(mean = apply(dplyr::select(., select_vars), 1, mean)) %>%
    mutate(median = apply(dplyr::select(., select_vars), 1, median)) %>%
    mutate(var = apply(dplyr::select(., select_vars), 1, var)) %>%
    mutate(sd = apply(dplyr::select(., select_vars), 1, sd)) %>%
    mutate(mean_inf = mean - sd) %>%
    mutate(mean_sup = mean + sd) %>%
    rename(annee = derniere) %>%
    mutate(normal_inf = if_else(annee < mean_inf, "inf", "")) %>%
    mutate(normal_sup = if_else(annee > mean_sup, "sup", "")) %>%
    mutate(tendance = sprintf("%s%s", normal_inf, normal_sup)) %>%
#    filter(tendance != "") %>%
#    mutate(annee = sprintf("%0.2f", annee)) %>%
#    mutate(moyenne = sprintf("%0.2f", mean)) %>%
    rename(moyenne = mean) %>%
    mutate(tolerance = sprintf("%0.2f %0.2f", mean_inf, mean_sup)) %>%
    dplyr::select(espece, annee, moyenne, tolerance, tendance) %>%
    arrange(-annee) %>%
    glimpse()
  colnames(df)[colnames(df) == "annee"] <- derniere
  return(invisible(df))
}