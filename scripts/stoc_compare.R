# <!-- coding: utf-8 -->
#
# quelques fonctions pour le protocole stoc
# auteur : Marc Gauthier
# licence: Creative Commons Paternité - Pas d'Utilisation Commerciale - Partage des Conditions Initiales à l'Identique 2.0 France
#
#
#
## comparaison entre deux sources de données -------------------------
#
# comparaison pour des id
# source("geo/scripts/stoc.R");compare_id()
compare_id <- function(force = TRUE, donnees1 = "mnhn_brut", donnees2 = "faune_proto", annees = "2019" ) {
  carp("début")
  library(janitor)
  library(tidyverse)
  library(compareDF)
  library(knitr)
  library(kableExtra)
#  annees <- c("2016", "2017", "2018", "2019")
#  annees <- c("2016", "2017", "2018")
#  annees <- c("2018")
  df1 <- mnhn_brut_lire(force) %>%
    mutate(annee = sprintf("%s", annee)) %>%
    filter(! etude %in% c("STOC_ONF")) %>%
#    glimpse() %>%
    filter(annee %in% annees) %>%
#    filter(id %in% ids) %>%
    dplyr::select(id, horodatage, nom_francais, distance = distance_contact) %>%
    arrange(id, horodatage, nom_francais, distance) %>%
    glimpse()
  if (nrow(df1) == 0 ){
    exit
  }
  df2 <- faune_proto_lire(force = force, detail = TRUE) %>%
#    glimpse() %>%
    filter(! nom_francais %in% c(
      "Bergeronnette grise (M.a.alba)",
      "Goéland argenté (L.a.argenteus)",
      "Canard domestique (origine non naturelle)"
    )) %>%
    filter(annee %in% annees) %>%
#    filter(id %in% ids) %>%
#    glimpse() %>%
    dplyr::select(id, horodatage, nom_francais, distance) %>%
    arrange(id, horodatage, nom_francais, distance) %>%
    glimpse()
  df3 <- df1 %>%
    group_by(id, horodatage) %>%
    summarize(nb = n())
  df4 <- df2 %>%
    group_by(id, horodatage) %>%
    summarize(nb = n())
  df5 <- df3 %>%
    full_join(df4, by = c("id", "horodatage")) %>%
    arrange(id, horodatage) %>%
    filter(nb.x != nb.y)
  print(knitr::kable(df5, format = "pipe"))
  df1$source <- 'mnhn'
  df2$source <- 'faune'
  df5 <- df1 %>%
    full_join(df2, by = c("id", "horodatage", "nom_francais", "distance")) %>%
    arrange(id, horodatage, nom_francais, distance) %>%
    filter(is.na(source.x) | is.na(source.y))
  print(knitr::kable(df5, format = "pipe"))
}
#
# source("geo/scripts/stoc.R");compare_liste()
compare_liste <- function(filtre = "350431_2018,2018-03-11 08:04") {
  library(stringr)
  filtres <- strsplit(filtre, ",")[[1]] %>%
    glimpse()
  ids <- filtres[1]
  horodatage <- filtres[2]
  carp("ids: %s horodatage: %s", ids, horodatage)
  df1 <- mnhn_brut_lire(force = FALSE) %>%
    filter(id %in% ids) %>%
    filter(horodatage == !!horodatage) %>%
    dplyr::select(espece = nom_francais, distance = distance_contact, code_observation)  %>%
    arrange(espece)
  df2 <- faune_proto_lire(force = FALSE) %>%
    filter(id %in% ids) %>%
    filter(horodatage == !!horodatage) %>%
    dplyr::select(espece = name_species, Detail, distance, id_sighting) %>%
    arrange(espece)
  df3 <- df1 %>%
    full_join(df2, by = c("espece", "distance")) %>%
    arrange(espece)
  print(knitr::kable(df3, format = "pipe"))
  return()
  mnhn_liste(df1)
  faune_liste(df2)

  champs <- colnames(df1)
#  df3 <- compare_df(df1, df2, champs)
}
# source("geo/scripts/stoc.R");compare_liste_faune(ids = c("350376_2017"))
compare_liste_faune <- function(ids = c("350015_2017")) {
  df1 <- mnhn_brut_lire(force = FALSE) %>%
    filter(id %in% ids) %>%
    group_by(id, aaaammjj) %>%
    summarize(nb = n())
  View(df1)
  df2 <- faune_proto_lire(force = FALSE) %>%
    filter(id %in% ids) %>%
    group_by(id, aaaammjj) %>%
    summarize(nb = n())
  View(df2)
}